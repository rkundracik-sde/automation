#!/bin/bash -eo pipefail

echo "This script opens up documentation for the dependencies in the automation repository."
AUTOMATION_LIBRARIES=$(cat requirements.txt | sed '/^#/d' | awk '{split($0,a," "); print a[1]}')
echo "Enter the number of the url you want to open for api documentation:"

PS3="Automation Documentation: "

select url in $AUTOMATION_LIBRARIES
do
	if [ ! -z $url ]; then
        API_DOCS=$(pip show $url | grep "Home-page" | awk '{split($0,a," "); print a[2]}')
        if [ -z "$API_DOCS" ]; then
            echo "Could not find the docs for that library"
            exit 1
        fi
        OPERATING_SYSTEM=$(uname -s)
        if [[ $OPERATING_SYSTEM == "Darwin" ]]; then
		    open $API_DOCS
        else
            echo $API_DOCS
        fi
		break
	else
		echo ""
		echo "You did not pick a valid option!"
		break
	fi
done
