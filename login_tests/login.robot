*** Settings ***
Documentation     Login into Connect FoodLogiQ.
Library           SeleniumLibrary

*** Variables ***
${LOGIN URL}      https://connect.foodlogiq.com
${BROWSER}        Chrome

*** Test Cases ***
Valid Login
    Open Browser To Login Page
    Input Username    %{AUTOMATION_USERNAME}
    Input Password    %{AUTOMATION_PASSWORD}
    Submit Credentials
    [Teardown]    Close Browser

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Title Should Be    FoodLogiQ

Input Username
    [Arguments]    ${username}
    Input Text    email    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button    loginButton
