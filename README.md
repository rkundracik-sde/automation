# automation

## Description

A FoodLogiQ repository focused on building an Acceptance Test Suite using Robot Framework

[![Build Status](https://travis-ci.com/FoodLogiQ/automation.svg?token=qy2ikmMVpyxVEGTyrYja&branch=adding-initial-linting-and-automation)](https://travis-ci.com/FoodLogiQ/automation)

## Project Setup

For this project you need to have python 2 installed. 
You can check that you have python installed with the following command:

```bash
python --version
Python 2.7.15
```

Your python binary executable should report that you have python 2 installed which in this case is 2.7.15

## Installing Python Dependencies

There are 3 Python Library Dependencies in this project:

* [Robot Framework](http://robotframework.org/)
* [Robot Framework Selenium Library](https://github.com/robotframework/SeleniumLibrary)
* [pytest](https://docs.pytest.org/en/latest/)
* [pylint](https://github.com/PyCQA/pylint)
* [autopep8](https://github.com/hhatto/autopep8)

Run `make install` or simply `make` to install all the dependencies using [pip](https://pip.pypa.io/en/stable/)

## View Documentation for the Libraries

#### Api Documentation

Run `make show-api-docs` to see the documentation for selenium

#### View Travis Build

In order to view the current builds in Travis CI run `make travis-open`

## Installing TravisCI CLI

You can install travis ci cli in order to lint the `.travis.yml` file and more

Run this command to install travisci cli: `gem install travis`

#### Lint the travis yml script

Run this command to lint the travis yml script: `make lint-travis`

## Linting Python code

Linting to be done throughout codebase

#### Fixing Linting Issues

Use the [autopep8](https://pypi.org/project/autopep8/) tool to automate fixing linting issues:

You can run the following command to automatically lint a python file but it is an aggressive linting rule so use at your discretion:

`make lint-fix` this Makefile task does the following command for each python file:

`autopep8 --in-place --aggressive --aggressive *.py`

## Running Robot Framework locally

You can test the robot framework acceptance test suit locally by running the following command

```
robot --variable "USERNAME:${AUTOMATION_USERNAME}" --variable "PASSWORD:${AUTOMATION_PASSWORD}" login_tests ...
```

You can run the following Makefile task:

```
AUTOMATION_USERNAME="someUsername" AUTOMATION_PASSWORD="SomePassword" make run-robot
```

You can also set these environment variables in your *.bashrc,.zshrc,.profile* as well.
