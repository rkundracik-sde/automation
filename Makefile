install:
	pip install -r requirements.txt

lint:
	pylint *.py

lint-fix:
	find . -name "*.py" -exec autopep8 --in-place --aggressive --aggressive {} \;

install-travis: 
	gem install travis

lint-travis:
	travis lint .travis.yml

open-travis:
	travis open

show-api-docs:
	bash scripts/docs.sh

run-robot:
	robot --variable "USERNAME:${AUTOMATION_USERNAME}" --variable "PASSWORD:${AUTOMATION_PASSWORD}" login_tests

clean-robot-generated-files:
	find . -name "*.html" -exec rm -f {} \;
	find . -name "*.xml" -exec rm -f {} \;
	find . -name "*.png" -exec rm -f {} \;

clean-pyc:
	find . -name "*.pyc" -exec rm -f {} \;
	find . -name "*.pyo" -exec rm -f {} \;
