"""
A Sample Test File
"""


def summation(number):
    """This function simply adds one"""
    return number + 1


def test_answer():
    """Unit Test Case"""
    assert summation(4) == 5
